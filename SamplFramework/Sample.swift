//
//  Sample.swift
//  SamplFramework
//
//  Created by sukhmeet on 2016-08-13.
//  Copyright © 2016 SYN. All rights reserved.
//

import Foundation

public class Sample {
    
    public init () {
        print("Created Sample Object")
    }
    
    public func Log(string : String){
        print(string)
    }
    
}
